#include <stdio.h>

struct employee
{
    char name[50];
    int number;
    float salary;
};

typedef struct employee Employee;

Employee input()
{
    Employee e;

    printf("Enter the employee name: ");
    scanf("%s",e.name);
    printf("Enter employee number: ");
    scanf("%d",&e.number);
    printf("Enter salary: ");
    scanf("%f",&e.salary);

    return e;
}

void output(Employee e)
{
    printf("Name: %s\n",e.name);
    printf("Employee number: %d\n",e.number);
    printf("Salary: %f\n",e.salary);
    printf("\n");
}

int main()
{
    int n;

    printf("Enter the number of employees: ");
    scanf("%d",&n);

    Employee employee[n];

    for(int i=0;i<n;i++)
    {
        employee[i]=input();
    }

    for(int i=0;i<n;i++)
    {
        output(employee[i]);
    }
    
    return 0;
}
