#include <stdio.h>

void input(char str[])
{
    printf("Enter a string: ");
    scanf("%s",str);
}

void concate(char str1[] ,char str2[],char str3[])
{
    int i=0,j=0;
    
    while(str1[i]!='\0')
    {
        str3[j]=str1[i];
        j++;
        i++;
    }
    
    i=0;
    
    while(str2[i]!='\0')
    {
        str3[j]=str2[i];
        j++;
        i++;
    }
    
    str3[j]='\0';
}

void output(char str1[],char str2[],char str3[])
{
    printf("%s + %s = %s\n",str1,str2,str3);
}

int main()
{
    int n;
    
    printf("Enter max length of string: ");
    scanf("%d",&n);
    
    char str1[n],str2[n],str3[2*n];
    
    input(str1);
    input(str2);
    concate(str1,str2,str3);
    output(str1,str2,str3);
    
    return 0;
}