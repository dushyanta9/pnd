#include <stdio.h>
struct three
{
	int a,b,c;
};

typedef struct three Three;

Three input();
Three calculate(Three p);

Three input()
{
	Three p;
	printf("Enter the three numbers:\n");
	scanf("%d%d%d",&p.a,&p.b,&p.c);
	return p;
}

Three calculate(Three p)
{
	Three m;
	
	if(p.a>p.b)
	{
	m.a=p.a;
	m.b=p.b;
	}
	else
	{
	m.a=p.b;
	m.b=p.a;
	}
	
	if(m.a<p.c)
	m.a=p.c;
	
	if(m.b>p.c)
	m.b=p.c;
	
	m.c=p.a+p.b+p.c;
	
	return m;
}

void output(Three p)
{
	printf("The smallest number is: %d and largest is: %d\n",p.b,p.a);
	printf("The total is: %d and average is: %f\n",p.c,(float)p.c/3);
}

int main()
{
	Three p=input();
	p=calculate(p);
	output(p);
	
	return 0;
}

	
 
 