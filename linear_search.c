#include <stdio.h>
int input(int n,int arr[])
{
    printf("Enter the elements of array:\n");
    
    for(int i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
    }
    
    printf("enter the element to be searched: ");
    int k;
    scanf("%d",&k);
    
    return k;
}

int search(int n,int k,int arr[])
{
    int flag=-1;
    
    for(int i=0;i<n;i++)
    {
        if(arr[i]==k)
        {
            flag=i;
            break;
        }
    }
    
    return flag;
}

void output(int k,int flag)
{
    if(flag!=-1)
    printf("The %d number is found at %d position of array.\n",k,flag);
    else
    printf("The %d number was not found.\n",k);
    
}

int main()
{
    int n,k,flag;
    
    printf("Enter value of n: ");
    scanf("%d",&n);
    
    int arr[n];
    
    k=input(n,arr);
    flag=search(n,k,arr);
    output(k,flag);
    
    return 1;
}
    
    