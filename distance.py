import math
def ipt():
    x1=int(input("Enter the X co-ordinates:"))
    y1=int(input("Enter the Y co-ordinates:"))
    
    return x1,y1;
    
def calculate(x1,x2,y1,y2):
    x1=x2-x1
    y1=y2-y1
    
    x1=x1**2
    y1=y1**2
    
    d=math.sqrt(x1+y1)
    
    return d;

def output(x1,x2,y1,y2,d):
    print("(",x1,",",y1,") + (",x2,",",y2,") = ",d)
    
x1,y1=ipt()
x2,y2=ipt()

d=calculate(x1,x2,y1,y2)

output(x1,x2,y1,y2,d)