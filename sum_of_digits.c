#include <stdio.h>

int input()
{
    int n;
    printf("Enter a number:\n");
    scanf("%d",&n);
    return n;
}

int sum(int n)
{
    int s=0;
    while(n!=0)
    {
        s+=n%10;
        n=n/10;
    }
    return s;
}

void output(int s)
{
    printf("Sum of digits:%d\n",s);
}
int main()
{
    int n,s;

    n=input();
    s=sum(n);
    output(s);

    return 0;
}