#include <stdio.h>

struct fraction
{
    int num,den;
};
typedef struct fraction Fraction;

int gcd(int m, int n);


void input(int n, Fraction arr[])
{
        for(int j=0;j<n;j++)
    {
        printf("Enter the fraction in A/B for. eg) 23/7 :");
        scanf("%d/%d",&arr[j].num,&arr[j].den);
    }
}

int gcd(int m,int n)
{
    
   int k;
    
    while (n!=0)
    {
        k=n;
        n=m%n;
        m=k;
    }
        
    return m;
            
}

Fraction reduce(Fraction frac)
{
   int g=gcd(frac.num,frac.den);
   
   frac.num=frac.num/g;
   frac.den=frac.den/g;
   
   return frac;
}


Fraction calculate(int n,Fraction arr[])
{
    Fraction frac=arr[0];
    
    for(int i=1;i<n;i++)
    {
        frac.num=(frac.num*arr[i].den)+(frac.den*arr[i].num);
        frac.den=(frac.den*arr[i].den);
        
        frac=reduce(frac);
    }
    return frac;
}

void output(int n, Fraction frac, Fraction arr[])
{
    for(int i=0;i<n-1;i++)
    {
        printf("%d/%d +",arr[i].num,arr[i].den);
    }
    
    printf("%d/%d = %d/%d\n",arr[n-1].num,arr[n-1].den,frac.num,frac.den);
}

int main()
{
    printf("Enter value of n:\n");
    int n;
    scanf("%d",&n);
    
    Fraction arr[n],frac;
    
    input(n,arr);
    frac=calculate(n,arr);
    output(n,frac, arr);
    return 0;
}