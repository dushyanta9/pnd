#include <stdio.h>

struct position
{
    int max,min;
};

typedef struct position Position;
Position compute(int arr[],int n);

void input(int arr[],int n)
{
    printf("Enter the elements:");
    
    for(int i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
    
    }
}

Position compute(int arr[],int n)
{
    int ma=arr[0],mb=arr[0];
    Position p;
    p.max=0;
    p.min=0;
    
    for(int i=1;i<n;i++)
    {
        if(ma<arr[i])
        {
            ma=arr[i];
            p.max=i;
        }
        
        if(mb>arr[i])
        {
            mb=arr[i];
            p.min=i;
        }
    }
     
     int t=arr[p.max];
    arr[p.max]=arr[p.min];
    arr[p.min]=t;
    
     return p;
}
        

void output(int arr[],int n,Position p)
{
    printf("Smallest number is: %d and its position: %d\n",arr[p.max],p.max);
    printf("Largest number is: %d and its position: %d\n",arr[p.min],p.min);
    
    for(int i=0;i<n-1;i++)
    {
        printf("%d, ",arr[i]);
    }
    printf("%d",arr[n-1]);
}

int main()
{
    int n;
    Position p;
    
    printf("Enter value of n: ");
    scanf("%d",&n);
    
    int arr[n];
    
    input(arr,n);
    p=compute(arr,n);
    output(arr,n,p);
    
    return 0;
}
    