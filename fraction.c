#include <stdio.h>

struct fraction
{
    int num,den;
    
};
typedef struct fraction Fraction;

int gcd(int m,int n);

Fraction input()
{
    Fraction frac;
    printf("Enter the fraction in A/B form, eg- 23/9\n");
    scanf("%d/%d",&frac.num,&frac.den);
    return frac;
}

Fraction calculate(Fraction frac1,Fraction frac2)
{
    frac1.num=(frac1.num*frac2.den)+(frac2.num*frac1.den);
    frac1.den=frac1.den*frac2.den;
    
    return frac1;
}

Fraction reduce(Fraction frac)
{
   int g=gcd(frac.num,frac.den);
   
   frac.num=frac.num/g;
   frac.den=frac.den/g;
   
   return frac;
}

int gcd(int m,int n)
{
    int k;
    
    while (n!=0)
    {
        k=n;
        n=m%n;
        m=k;
    }
        
    return m;
            
}

void output(Fraction frac,Fraction frac1,Fraction frac2)
{
    printf("%d/%d + %d/%d = %d/%d\n",frac1.num,frac1.den,frac2.num,frac2.den,frac.num,frac.den);
}

int main()
{
    Fraction frac1=input();
    Fraction frac2=input();
    Fraction frac;
    
    frac=calculate(frac1,frac2);
    frac=reduce(frac);
    
    output(frac,frac1,frac2);
    return 0;
}