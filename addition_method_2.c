#include <stdio.h>
int input()
{
    int r;
    printf("Enter a number\n");
    scanf("%d", &r);
    return r;
}

int add(int x, int y)
{
    int r=x+y;
    return r;
}

int output(int s)
{
    printf("The sum is:%d", s);
    return 0;
}

int main()
{
    int a=input();
    int b=input();
    
    int sum=add(a,b);
    int k=output(sum);
    return k;
}