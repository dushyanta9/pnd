#include <stdio.h>

struct information
{
    char name[50];
    float marks;
    int id;
    float fee;
};

typedef struct information info;

info input()
{
    info s;
    
    printf("Enter student name: ");
    scanf("%s",s.name);
    printf("Enter student ID: ");
    scanf("%d",&s.id);
    printf("Enter student fee: ");
    scanf("%f",&s.fee);
    printf("Enter student marks: ");
    scanf("%f",&s.marks);
    printf("\n");
    
    return s;
}

info compare(info s1,info s2)
{
    info s;
    
    if(s1.marks>s2.marks)
    s=s1;
    else
    s=s2;
    
    return s;
}

void output(info s)
{
    printf("Name: %s\n",s.name);
    printf("ID: %d\n",s.id);
    printf("Fee: %f\n",s.fee);
    printf("Marks: %f\n",s.marks);
}

int main()
{
    info s1,s2;
    
    s1=input();
    s2=input();
    
    s1=compare(s1,s2);
    
    output(s1);
    
    return 0;
}
