#include <stdio.h>

void input(char str[])
{
    printf("Enter the string: ");
    scanf("%s",str);
}

void output(char str[])
{
    printf("%s\n",str);
}

void file_write(int n)
{
    FILE *fp;
    
    fp=fopen("input.txt","w");
    
    char str[n];
    
    input(str);
    
    fputs(str,fp);
    
    fclose(fp);
}

void file_read(int n)
{
    FILE *fp;
    
    fp=fopen("input.txt","r");
    
    char str[n];
    
    fgets(str,n,fp);
    
    output(str);
    
    fclose(fp);
    
}
    
    
    

int main()
{
    int n;
    
    printf("Enter max size of string: ");
    scanf("%d",&n);
    
    file_write(n);
    file_read(n);
    
    return 0;
    
}

