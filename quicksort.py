def quicksort(arr,init,fnl):

    if(init >= fnl-1):
        return;

    counter=0

    for i in range(init,fnl-1):

        if(arr[i]<arr[fnl-1]):
            arr[counter],arr[i]=arr[i],arr[counter]
            counter+=1

    arr[counter],arr[fnl-1]=arr[fnl-1],arr[counter]

    quicksort(arr,init,counter-1)
    quicksort(arr,counter+1,fnl-1)

arr=[1,6,23,67,2]

print(arr[:])

quicksort(arr,0,len(arr))

print(arr[:])
