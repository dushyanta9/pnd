#include <stdio.h>

float input()
{
    float n;
    printf("Enter a number: ");
    scanf("%f",&n);
    
    return n;
}

void swap(float *x,float *y)
{
    int t=*x;
    *x=*y;
    *y=t;
}

void output(float x,float y)
{
    printf("%f , %f\n",x,y);
}

int main()
{
    float x,y;
    
    x=input();
    y=input();
    
    output(x,y);
    
    swap(&x,&y);
    
    output(x,y);
    
    return 0;
}