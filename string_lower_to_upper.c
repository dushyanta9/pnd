#include <stdio.h>

void input(char str[])
{
    printf("Enter a string: ");
    scanf("%s",str);
}

void upper_case(char str1[],char str2[])
{
    for(int i=0;str1[i]!='\0';i++)
    {
        if(str1[i]>=97 && str1[i]<=122)
        str2[i]=str1[i]-32;
        else
        str2[i]=str1[i];
    }

}

void lower_case(char str1[],char str2[])
{
    for(int i=0;str1[i]!='\0';i++)
    {
        if(str1[i]>=65 && str1[i]<=91)
        str2[i]=str1[i]+32;
        else
        str2[i]=str1[i];
    }

}


void output(char str1[],char str2[],char str3[])
{
    printf("'%s' in Upper case is '%s' \n",str1,str2);
    printf("'%s' in Lower case is '%s' \n",str1,str3);
}

int main()
{
    int n;
    printf("Enter max size of string: ");
    scanf("%d",&n);
    
    char str1[n],str2[n],str3[n];
    
    input(str1);
    upper_case(str1,str2);
    lower_case(str1,str3);
    output(str1,str2,str3);
    
    return 0;
}