#include <stdio.h>

void input(int arr[],int n)
{
    printf("Enter the elements\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
    }
}

void output(int arr[],int n)
{
    printf("The elements of array are:\n");
    
    for(int i=0;i<n-1;i++)
    {
        printf("%d,",arr[i]);
    }
    
    printf("%d\n",arr[n-1]);
}

int main()
{
    int n;
    printf("Enter value of n:");
    scanf("%d",&n);
    
    int arr[n];
    
    input(arr,n);
    output(arr,n);
    
    return 0;
}
