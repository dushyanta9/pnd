#include <stdio.h>

int main()
{
    char str[100];
    
    printf("Enter the string: ");
    gets(str);
    
    int i=0;
    
    while(1)
    {
        if(str[i]!=0)
        i++;
        else
        break;
    }
    
    printf("The length of string '%s' is %d.\n",str,i);
    
    return 0;
}