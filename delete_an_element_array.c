#include <stdio.h>

void input(int n,int arr[])
{
    printf("Enter the elemnts of array:\n");
    
    for(int i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
    }
}

void delete(int n,int h, int arr[])
{
    int arr2[h],k;
    for(int i=0;i<h;i++)
    {
        printf("Enter position of %d elements that has to be deleted:",i+1);
        scanf("%d",&arr2[i]);
    }
    
    for(int i=0;i<h;i++)
    {
        for(int j=arr2[i];j<n-i-1;j++)
        {
            arr[j]=arr[j+1];
        }
    }
}

void output(int n,int arr[])
{
    for(int i=0;i<n-1;i++)
    {
        printf("%d(%d),",arr[i],i);
    }
    printf("%d(%d)\n",arr[n-1],n-1);
}

int main()
{
    int n,h;
    printf("Enter the value of n:");
    scanf("%d",&n);
    
    int arr[n];
    input(n,arr);
    
    printf("Enter the amount of elements to be deleted:");
    scanf("%d",&h);
    
    if(h>n)
        printf("Error:deleting more elements than array size\n");
    else
    {
        printf("This is the array with position of elements written is parenthesis:\n");
        output(n,arr);
        delete(n,h,arr);
        printf("Array after deletion of element is:");
        output(n-h,arr);
    }
    return 0;
}