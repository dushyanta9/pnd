#include <stdio.h>
 void input(int arr[],int n)
 {
     printf("Enter the elements\n");
     
     for(int i=0;i<n;i++)
     {
         scanf("%d",&arr[i]);
     }
}

int average(int arr[],int n)
{
    int sum=0;
    
    for(int i=0;i<n;i++)
    {
        sum+=arr[i];
    }
    
    return sum;
}

void output(int arr[],int n,int sum)
{
    printf("%d",arr[0]);
    for(int i=1;i<n;i++)
    {
        printf(" + %d",arr[i]);
    }
    
    printf(" = %d\n",sum);
}

int main()
{
    int n;
    printf("enter the value of n:");
    scanf("%d",&n);
    
    int arr[n];
    
    input(arr,n);
    int sum=average(arr,n);
    output(arr,n,sum);
    
    return 0;
}