#include <stdio.h>

int input()
{
    int a;
    printf("Type a number: ");
    scanf("%d", &a);
    
    return a;
}

int largest(int a,int b, int c)
{
    if(b>a)
    a=b;
    
    if(c>a)
    a=c;
    
    return a;
}

void output(int a,int b,int c,int l)
{
    printf("Largest of %d,%d,%d is %d\n",a,b,c,l);
}

int main()
{
    int a,b,c,l;
    
    a=input();
    b=input();
    c=input();
    
    l=largest(a,b,c);
    
    output(a,b,c,l);
    
    return 0;
}
 