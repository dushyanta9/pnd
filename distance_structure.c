#include <stdio.h>
#include <math.h>
struct points
{
    float x;
    float y;
    float m;
};

int main()
{
    struct points p1,p2;
    printf("Enter co-ordinate of first point:\n");
    scanf("%f%f", &p1.x,&p1.y);
    
    printf("Enter co-ordinate of second point:\n");
    scanf("%f%f", &p2.x,&p2.y);
    
    p1.m=pow((p1.x-p2.x),2);
    p2.m=pow((p1.y-p2.y),2);
    
    float s=sqrt(p1.m+p2.m);
    
    printf("Distance is:%f\n",s);
    return 0;
}
    

    
    