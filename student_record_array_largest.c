#include <stdio.h>

void input(int arr[5][3])
{
    printf("Enter the details:\n");

    for(int i=0;i<5;i++)
    {
        printf("Enter marks of %d student:\n",i+1);

        for(int j=0;j<3;j++)
        {
            scanf("%d",&arr[i][j]);
        }
    }
}

int higest(int arr[5][3],int student[5],int marks[3])
{
    int high;
    int sum[5]={0};
    
    for(int i=0;i<5;i++)
    {
        high=arr[i][0];
        sum[i]+=arr[i][0];
        
        for(int j=1;j<3;j++)
        {
            sum[i]+=arr[i][j];
            
            if(arr[i][j]>high)
            high=arr[i][j];
        }
        student[i]=high;
    }
    
    for(int i=0;i<3;i++)
    {
        high=arr[0][i];
        
        for(int j=1;j<5;j++)
        {   
            if(arr[j][i]>high)
            high=arr[i][j];
        }
        marks[i]=high;
    }
    
    high=sum[0];
    
    for(int i=1;i<5;i++)
    {
        if(sum[i]>high)
        high=sum[i];
    }
    
    return high;
}

void output(int student[],int marks[],int high)
{
    for(int i=0;i<5;i++)
    {
        printf("Higest marks of %d student: %d\n",i+1,student[i]);
    }
    
    for(int i=0;i<3;i++)
    {
        printf("Student with higest marks in %d subject: %d\n",i+1,marks[i]);
    }
    
    printf("Higest marks scored by a student: %d\n",high);
}
    


int main()
{
    int arr[5][3],student[5],marks[3],high;
    
    input(arr);
    high=higest(arr,student,marks);
    output(student,marks,high);
    
    return 0;
}
