#include <stdio.h>

int input(int n,int arr[])
{
    int k;
    printf("Enter elements of array:\n");
    
    for(int i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
    }
    
    printf("Enter element to be searched: ");
    scanf("%d",&k);
    
    return k;
}

void arrange(int n,int arr[])
{
    int k,p;
    
    for(int i=0;i<n;i++)
    {
        k=arr[i];
        p=i;
        
        for(int j=i;j<n;j++)
        {
            if(k>arr[j])
            {
                p=j;
                k=arr[j];
            }
        }
        
        arr[p]=arr[i];
        arr[i]=k;
    }
}

int search(int n,int k,int arr[])
{
    int a=0,b=n-1,flag=-1;
    
    while(a<=b)
    {
        int c=(a+b)/2;
        
        if(k>arr[c])
        a=c+1;
        else
        if(k<arr[c])
        b=c-1;
        else
        if(k==arr[c])
        {
            flag=c;
            break;
        }
    }
    
    return flag;
}
        
        

void output(int k,int flag)
{
    if(flag!=-1)
    printf("The number %d is found.\n",k);
    else
    printf("The number %d was not found.\n",k);
    
}

int main()
{
    int n,k,flag;
    
    printf("Enter size of array: ");
    scanf("%d",&n);
    
    int arr[n];
    
    k=input(n,arr);
    arrange(n,arr);
    flag=search(n,k,arr);
    output(k,flag);
    
    return 0;
}
    