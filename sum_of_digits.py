def ipt():
    n=input("Enter a number: ")
    return n;
    
def digits(n):
    
    s=0
    
    while n!=0:
        a=int(n % 10)
        s+=a
        n/=int(10)
        
    return s;
    
def output(n,s):
    print("Sum of digits of ",n,"is: ",s)

n=ipt()
s=digits(int(n))
output(n,s)