#include <stdio.h>
struct time
{
    int a,b;
};

typedef struct time helper;

helper input();

helper input()
{
    helper p;
    printf("Enter time in X:Y format, eg- 12:38\n");
    scanf("%d:%d",&p.a,&p.b);
    return p;
}

int convertor(helper p)
{
    int min=(p.a*60)+p.b;
    return min;
}

void output(int min)
{
    printf("Time in minutes is: %d minutes\n",min);
}

int main()
{
    helper t;
    t=input();
    int m=convertor(t);
    output(m);
}