#include <stdio.h>

int arranging(int arr[],int n)
{
    int pivot=n-1;
    int counter=0;
    
    for(int i=0;i<n;i++)
    {
        if(arr[i]<arr[pivot])
        {
            int t=arr[counter];
            arr[counter]=arr[i];
            arr[i]=t;
            
            counter++;
        }
    }
    
    int t=arr[pivot];
    arr[pivot]=arr[counter];
    arr[counter]=t;
}

int main()
{
     int arr[]={8,6,23,7,4,7,34,2,54,78,4,15};
     int n=12;
    
     arranging(arr,n);
     
     for(int i=0;i<n;i++)
         {
             printf("%d,",arr[i]);
         }
     
     return 0;
}