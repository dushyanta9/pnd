#include <stdio.h>

void input(int arr[],int n)
{
    for(int i=0;i<n;i++)
        scanf("%d",&arr[i]);
}

int sum(int arr[], int n)
{
    int s=0;
    for(int i=0;i<n;i++)
    {
        s+=arr[i];
    }   
        return s;
}

void output(int s)
{
    printf("The sum is:%d\n",s);
}

int main()
{
    int n;
    printf("Enter value of n:\n");
    scanf("%d",&n);
    int arr[n];
    
    printf("Enter the numbers:\n");
    input(arr,n);
    
    int su=sum(arr,n);
    output(su);
        
   return 0;
    
}