def ipt(arr):
    
    for i in range(5):
        print("Enter marks of ",i+1," student: ",sep="")
        
        for j in range(3):
            arr[i][j]=int(input("Enter marks: "))
    
    return;
    
def maximum(arr,marks):
    
    for i in range(3):
        l=0
        for j in range(5):
        
            if (arr[j][i]>l):
                l=arr[j][i]
        
        marks[i]=l
        
    return;
    
def output(marks):

    for i in range(3):
        print("Max mark in subject ",i+1," is: ",marks[i],sep="")

arr=[[None]*3]*5
marks=[None]*3

ipt(arr)
maximum(arr,marks)
output(marks)
        
    