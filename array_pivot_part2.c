#include <stdio.h>

int arranging(int arr[],int n)
{
    int pivot=n-1;
    int counter=n-1;
    
    for(int i=0;i<n;i++)
    {
        if(arr[i]>arr[pivot])
        {
            counter=larger(arr,counter,i,pivot);
            
            if(counter!=-1)
            {
                int t=arr[counter];
                arr[counter]=arr[i];
                arr[i]=t;
                
            }
            else
            {
                int t=arr[pivot];
                arr[pivot]=arr[counter];
                arr[counter]=t; 
            }
            
        }
    }
    
    counter=larger(arr,n-1,-1,pivot);
    int t=arr[pivot];
    arr[pivot]=arr[counter+1];
    arr[counter+1]=t;
}

int larger(int arr[],int a,int b,int pivot)
{
    int flag=-1;
    for(int i=a;i>b;i--)
    {
        if(arr[i]<arr[pivot])
        {
            flag=i;
            break;
        }
    }
    
    return flag;
}

int main()
{
     int arr[]={8,6,23,7,4,7,34,2,54,78,4,15};
     int n=12;
    
     arranging(arr,n);
     
     for(int i=0;i<n;i++)
         {
             printf("%d,",arr[i]);
         }
     
     return 0;
}