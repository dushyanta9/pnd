#include <stdio.h>

struct fibonacci
{
    int n;
    char s1[200];
    char s2[200];
};

typedef struct fibonacci Fibonacci;

Fibonacci input()
{
    Fibonacci a;
    printf("Enter the number of terms: ");
    scanf("%d",&a.n);
    printf("Enter the first string without space or with white spaces: ");
    scanf("%s",&a.s1);
    printf("Enter the second string without space or with white spaces: ");
    scanf("%s",&a.s2);
    
    return a;
}

void concatenation(char s1[],char s2[],char s3[])
{
    int i=0,j=0;
    
    while(s1[i]!='\0')
    {
        s3[j]=s1[i];
        i++;
        j++;
    }
    
    i=0;
    
    while(s2[i]!='\0')
    {
        s3[j]=s2[i];
        i++;
        j++;
    }
    
    s3[j]='\0';
}

void replace(char s1[],char s2[],char s3[])
{
    int i=0;
    
    while(s2[i]!='\0')
    {
        s1[i]=s2[i];
        i++;
    }
    
    s1[i]='\0';
    
    i=0;
    
    while(s3[i]!='\0')
    {
        s2[i]=s3[i];
        i++;
    }
        

    s2[i]='\0';

}


void fibonacci_series(Fibonacci series)
{
    char s3[200];
    
    output(series.s1);
    output(series.s2);
    
    series.n-=2;
    
    while(series.n != 0)
    {
        concatenation(series.s1,series.s2,s3);
        output(s3);
        replace(series.s1,series.s2,s3);
        series.n--;
    }
    
}

void output(char s[])
{
    printf("%s\n",s);
}


int main()
{
    int m;
    
    printf("Enter the number of series: ");
    scanf("%d",&m);
    
    Fibonacci srs[m];
    
    for(int i=0;i<m;i++)
    {
        srs[i]=input();
    }
    
    for(int i=0;i<m;i++)
    {
        fibonacci_series(srs[i]);
        printf("\n");
    }
    
    return 0;
}