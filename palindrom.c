#include <stdio.h>

int input()
{
    int n;
    printf("Enter the number:\n");
    scanf("%d",&n);
    return n;
}

int reverse(int n,int t)
{
    int r=0;
    while(n!=0)
    {
    r=(r*10)+n%10;
    n=n/10;
    }
    return r;
}

void output(int n,int r)
{
    if(r==n)
    printf("The reverse of number is: %d and it is palindrome\n",r);
    else
    printf("The reverse of number is: %d and it is not palindrome\n",r);
    
}

int main()
{
    int n,r;
    
    n=input();
    r=reverse(n,n);
    output(n,r);
    
    return 0;
}