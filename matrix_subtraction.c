#include <stdio.h>

void input(int m,int n, int matrix[m][n])
{
    for(int i=0;i<m;i++)
    {
        printf("Enter %d row of matrix:\n",i+1);
        
        for(int j=0;j<n;j++)
        {
            scanf("%d",&matrix[i][j]);
        }
    }
}

void subtraction(int m, int n, int matrix1[m][n],int matrix2[m][n], int matrix3[m][n])
{
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
            matrix3[i][j]=matrix1[i][j]-matrix2[i][j];
        }
    }
}

void output(int m, int n, int matrix1[m][n],int matrix2[m][n],int matrix3[m][n])
{
    for(int i=0;i<m;i++)
    {
        printf("[");
        
        for(int j=0;j<n-1;j++)
        {
            printf("%d,",matrix1[i][j]);
        }
        printf("%d]\n",matrix1[i][n-1]);
    }
    
    printf("minus(-)\n");
    
    for(int i=0;i<m;i++)
    {
        printf("[");
        
        for(int j=0;j<n-1;j++)
        {
            printf("%d,",matrix2[i][j]);
        }
        printf("%d]\n",matrix2[i][n-1]);
    }
    
    printf("equals(=)\n");
    
    for(int i=0;i<m;i++)
    {
        printf("[");
        
        for(int j=0;j<n-1;j++)
        {
            printf("%d,",matrix3[i][j]);
        }
        printf("%d]\n",matrix3[i][n-1]);
    }
}

int main()
{
    int n,m;
    
    printf("Enter max row size of the array: ");
    scanf("%d",&m);
    printf("Enter max column size of array: ");
    scanf("%d",&n);
    
    int matrix1[m][n],matrix2[m][n],matrix3[m][n];
    
    input(m,n,matrix1);
    input(m,n,matrix2);
    
    subtraction(m,n,matrix1,matrix2,matrix3);
    
    output(m,n,matrix2,matrix2,matrix3);
    
    return 0;
}
    
    
    
    

            