def ipt():
    hour=int(input("Enter hours in 24 hour format:"))
    minute=int(input("Enter the minutes:"))
    
    return hour,minute;
    
def convert(hour,minute):
    minute=(hour*60)+minute
    
    return minute;
    
def output(hour, minute,conversion):
    print(hour,":",minute," = ",conversion,sep='')
    
    return;
    
hour,minute=ipt()
conversion=convert(hour,minute)
output(hour,minute,conversion)
    