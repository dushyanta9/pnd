#include <stdio.h>
#include <math.h>

struct point 
{
    float x,y;
};

typedef struct point Point;

Point input();
Point power(Point p1,Point p2);

Point input()
{
    Point p;
    printf("Enter the co-ordinates:\n");
    scanf("%f%f", &p.x,&p.y);

    return p;
}

Point power(Point p1, Point p2)
{
    Point p3;
    p3.x=pow(p1.x-p2.x,2);
    p3.y=pow(p1.y-p2.y,2);

    return p3;
}

float square(Point p)
{
    float s=sqrt(p.x+p.y);
    return s;
}

void output(float s)
{
printf("The distance between points is:%f\n",s);
}


int main()
{
    Point a,b;

    a=input();
    b=input();
    
    a=power(a,b);
    
    float c=square(a);
    
    output(c);
    return 0;
}
