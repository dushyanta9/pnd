#include <stdio.h>

void input(int m,int n,int matrix1[m][n],int matrix2[m][n])
{
    printf("Enter first matrix:\n");
    
    for(int i=0;i<m;i++)
    {
        printf("Enter elements of %d row:\n",i);
        
        for(int j=0;j<n;j++)
        {
            scanf("%d",&matrix1[i][j]);
        }
    }
    
    printf("Enter second matrix:\n");
    
     for(int i=0;i<m;i++)
    {
        printf("Enter elements of %d row:\n",i);
        
        for(int j=0;j<n;j++)
        {
            scanf("%d",&matrix2[i][j]);
        }
    }
}

void addition(int m,int n,int matrix1[m][n], int matrix2[m][n],int matrix3[m][n])
{
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
            matrix3[i][j]=matrix1[i][j]+matrix2[i][j];
        }
    }

}

void output(int m,int n,int matrix3[m][n])
{
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n-1;j++)
        {
            printf("%d,",matrix3[i][j]);
        }
    printf("%d\n",matrix3[i][n-1]);
    
    }
}

int main()
{
    int m,n;
    
    printf("Enter number of rows: ");
    scanf("%d",&m);
    printf("Enter number of columns: ");
    scanf("%d",&n);
    
    int matrix1[m][n],matrix2[m][n],matrix3[m][n];
    
    input(m,n,matrix1,matrix2);
    addition (m,n,matrix1,matrix2,matrix3);
    output(m,n,matrix3);
    
    return 0;
}