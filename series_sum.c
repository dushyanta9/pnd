#include <stdio.h>
int input()
{
    int n;
    printf("Enter value of n:\n");
    scanf("%d",&n);
    return n;
}

int factorial(int n)
{
    int s=1;
    for(int i=2;i<=n;i++)
    {
    s*=i;
    }

    return s;
}

void output(int n)
{
    printf(" + %d/%d!",n,n);
}


int main()
{
    int n;
    float sum=1.0;
    n=input();
    
    printf("1/1!");
    
    for(int i=2;i<=n;i++)
    {
    output(i);
    
    sum+=(float)i/factorial(i);
    }
    
    printf(" = %f\n",sum);
}