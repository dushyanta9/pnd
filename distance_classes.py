class details:
    def __init__(self):
        self.name=input("Enter employee name: ")
        self.id=int(input("Enter employee ID: "))
        self.salary=float(input("Enter employee salary: "))
    
def output(e):
    print("Employee name:",e.name)
    print("Employee ID:",e.id)
    print("Employee salary:",e.salary)
    return;

n=int(input("Enter the number of employees: "))

arr=[None]*n;

for i in range(n):
    arr[i]=details()
    
print("Employee details are: ")
    
for i in range(n):
    output(arr[i])
    