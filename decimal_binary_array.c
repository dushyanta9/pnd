#include <stdio.h>
#include <math.h>



int input()
{
    int n;
    printf("Enter a number: ");
    scanf("%d",&n);
    
    return n;
}

int size(int n)
{
    int k,i=1;
    while(i!=0)
    {
        k=pow(2,i);
        
        if(k>n)
        {
            k=i;
            break;
        }
        
        i++;
    }
    
    return k;
}

void conversion(int n,int sz,int arr[])
{
    
    for(int i=0;i<sz;i++)
    {
        arr[sz-1-i]=n%2;
        n/=2;
    }
}

void output(int n,int sz,int arr[])
{
    printf("%d in binary is: ",n);
    
    for(int i=0;i<sz;i++)
    {
        printf("%d",arr[i]);
    }
    
    printf("\n");
}

int main()
{
    int n,sz;
    
    n=input();
    sz=size(n);
    
    int arr[sz];
    
    conversion(n,sz,arr);
    output(n,sz,arr);
    
    return 0;
}
        
    
        